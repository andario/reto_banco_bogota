'use strict'

var express = require('express');
var Project = require('../models/project');

var controller = {
    home: function(req,res){
        return res.status(200).send({
            message: 'Soy la Home'
        });
    },
    test: function(req,res){
        return res.status(200).send({
            message: 'soy la Test'
        });
    },
    saveProject : function(req,res){
        var project = new Project();

        var params = req.body;
        project.name = params.name;
        project.description = params.description;
        project.category = params.category;
        project.year = params.year;
        project.langs = params.langs;
        project.image = null;

        project.save((err,projectStored)=>{
            if(err) return res.status(500).send({mesagge :'Error 500 al guardar '});
            if(!projectStored) return res.status(404).send({mesagge :'Error 404 al guardar '});        
            return res.status(200).send({project:projectStored, message:'Registro Guardado con exito'});
        });
    },

    getProject: function(req,res){
        var ProjectId = req.params.id;

        if(ProjectId == null){
            return res.status(404).send({message:'Error 404 al consultar Id'});
        }

        Project.findById(ProjectId, (err,project) => {

            if(err) return res.status(500).send({message:'Error 500 al consultar Id'});
            if(!project) return res.status(404).send({message:'Error 404 al consultar Id'});
            return res.status(200).send({
                project
            });
        } );
    },

    getProjects: function(req,res){
        Project.find({}).sort('-year').exec((err,projects) => {
            if(err) return res.status(500).send({message:'Error 500 al consultar los datos'});
            if(!projects) return res.status(404).send({message:'Error 500 No hay datos para mostrar'});
            return res.status(200).send({projects});
        })
    },

    updateProject: function(req,res){
        var ProjectId = req.params.id;
        console.log(ProjectId);
        var update = req.body;
        console.log(update);

        Project.findByIdAndUpdate( ProjectId , update ,{new:true},(err,projectUpdated) =>{
            if(err) return res.status(500).send({message:'Error 500 al actualizar los datos'});
            if(!projectUpdated) return res.status(404).send({message:'Error 404 No hay datos para actualizar'});
            return res.status(200).send({
                project : projectUpdated
            });
        });

    },

    deleteProject: function(req,res){
         var projectId= req.params.id;
         console.log(projectId);
         Project.findByIdAndDelete(projectId,( err ,projectRemove )=>{
         if(err) return res.status(500).send({message:'Error 500 al borrar los datos'});
         if(!projectRemove) return res.status(404).send({message:'Error 404 No hay datos para borrar'});
         return res.status(200).send({
            project : projectRemove
            });        
        });
    }
};

module.exports = controller;
