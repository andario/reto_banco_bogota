'use strict'

var  mongoose = require('mongoose');     //importamos el modulo de mongoose con require
var app = require('./app');
var port = 3700;


mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/portafolio')
    .then(()=>{
        console.log("*************************************************");
        console.log("Conexion a la base de datos establecida con exito");
        console.log("*************************************************");

        //creacion del servidor
        app.listen(port, () => {
            console.log("***********************************************************");
            console.log("servidor corriendo correctamente en la url : localhost:3700");
            console.log("***********************************************************");            
        })



    })
    .catch(err => console.log(err));