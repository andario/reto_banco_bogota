'use strict'

var express = require('express'); // cargamos el modulo express
var bodyParser = require('body-parser'); //// cargamos el modulo body-parser

var app = express();

//cargar archivos de rutas

var project_routes = require('./routes/project');

//middlewares se ejecuta antes de la ejecucion del resultado de una peticion
//en este caso configuramos el body-parse para entenderse en json
app.use(bodyParser.urlencoded({extended:false}));    //convertir a json todo las peticiones
app.use(bodyParser.json());                          

//CORS

//RUTAS

app.use('/api',project_routes);

//exportar
module.exports = app; //exporto la variable app 









