'use strict'

var express= require('express');
var ProjectControler= require('../controllers/project');

var router = express.Router();

router.get('/home',ProjectControler.home);
router.post('/test',ProjectControler.test);
router.post('/save-project',ProjectControler.saveProject);
router.get('/project/:id?',ProjectControler.getProject);
router.get('/projects',ProjectControler.getProjects);
router.put('/project/:id',ProjectControler.updateProject);
router.delete('/project/:id',ProjectControler.deleteProject);

module.exports=router;

